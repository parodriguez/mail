CONFIGURAR SERVIDOR MAIL
========================
POSTFIX
-------------------------------------------------
Para instalar POSTFIX en una distribución CentOs6 que fungira como servidor, como
usuario root hacemos
```shell
    yum install postfix
```

Ahora es necesario modificar el archivo de configuracion de postfix
```shell
    vi /etc/postfix/main.cf
```
Se tienen que modificar las siguientes lineas del archivo
```shell
    myhostname = mail.becarios.tonejito.info
    mydomain = becarios.tonejito.info
    inet_interfaces = all
    mynetworks = database.becarios.tonejito.info, database.priv.becarios.tonejito.info, 
            directory.becarios.tonejito.info, directory.priv.becarios.tonejito.info,
            mail.becarios.tonejito.info, mail.priv.becarios.tonejito.info,
            storage.becarios.tonejito.info, storage.priv.becarios.tonejito.info,
            web.becarios.tonejito.info, web.priv.becarios.tonejito.info
    home_mailbox = Maildir/
    mail_spool_directory = /srv/home/${USER}
    smtpd_use_tls = yes
    smtpd_tls_key_file = /etc/mail/ssl/privkey1.key
    smtpd_tls_cert_file = /etc/mail/ssl/privkey1.pem
```
Estas lineas modifican, respectivamente, el hostname del servidor, el dominio al
que pertenece el servidor, las interfaces por la que escucha la conexion 
(IPv4, IPv6), los DN a los que se permitira el envio de correos, la estructura del
directorio donde se almacenan los correos, el directorio donde se almacenan los
correos, y sobre el uso de certificados hechos en Let's Encrypt.

Consecuentemente, se modifica el archivo `master.cf`. Lo hacemos con el comando
```shell
    vi /etc/postfix/master.cf
```
Se descomentan las lineas siguientes
```shell
    smtp   inet   n - n - - smtpd
    submission inet n - n - - smtpd
```
para hacer validas todas las configuraciones hechas, es necesario reiniciar el servicio
```shell
    /etc/init.d/postfix restart
```
>En caso que salga un error al intentar iniciar el servicio, es necesario cambiar el 
>dueno del directorio `/var/spool/postfix`.
>Es necesario que el propietario sea el usuario postfix (creado por el mismo servicio).
>Para eso usamos
>```shell
>   chown -R postfix:postfix /var/spool/postfix
>```

Para montar el directorio compartido por el servidor `storage` primero se
necesita montar el directorio `/srv/home/` usando el siguiente comando
```shell
    mount -t nfs -o rw,nosuid storage.becarios.tonejito.info:/srv/home/ /srv/home
```
Posteriormente se tiene que editar el archivo `/etc/fstab` al que se le agrega la 
siguiente linea
```shell
    storage.becarios.tonejito.info:/srv/home /srv/home nfs rw,nosuid 0 0
```
 
>CONFIGURACION PARA CLIENTE POSTFIX 
 
Primero se instala postfix 
```shell
    apt-get  install postfix 
```

En la configuración inicial (en caso de tener una distribución de Debian)
se escojera los siguiente :
```shell

   General type of mail configuration 
   -----------------------------------
     Internet with smarthost
     
   Mail name
   -----------------------------------
     becarios.tonejito.com 
```
   
Para cualquier distribución, se debe modificar el archivo `/etc/postfix/main.cf`
con la siguiente configuración
```shell
    myhostname = <equipo>.becarios.tonejito.com
    mydomain = becarios.tonejito.info
    relayhost = mail.becarios.tonejito.com 
```
Finalmente se debe reiniciar el servicio Postfix
```shell
    /etc/init.d/postfix restart
```      
     
CONFIGURACION DE LDAP
---------------------------------------------------
Para lograr conexión con ldap fue necesario la instalación de los siguientes programas:
```shell
	yum install openldap-clients nss-pam-ldapd
```
NSLCD esta basado en Autenticación, para lo cual debemos realizar varios ajustes:

```shell
   authconfig –enableforcelegacy –update 
   authconfig --enableldap --enableldapauth --enablemkhomedir --ldapserver=ldap://directory.becarios.tonejito.info:389/ --ldapbasedn="dc=becarios,dc=tonejito,dc=info" --update
```

Para la correcta comunicación entre LDAP y el cliente debemos configurar 3 archivos:
```shell
/etc/openldap/ldap.conf

    BASE dc=directory,dc=becarios,dc=tonejito,dc=info
    URI ldap://directory.becarios.tonejito.info:389/
    
    TLS_CACERTDIR /etc/openldap/cacerts
    TLS_CACERT    /etc/openldap/cacerts/fullchain1.pem
    TLS_REQCERT demand
```
```shell
/etc/nsswitch.conf

    asswd:     compat ldap
    shadow:     compat ldap
    group:      compat ldap
    
    netgroup:   ldap
    
    aliases:    ldap
```
```shell
/etc/pam_ldap.conf

    base dc=directory,dc=becarios,dc=tonejito,dc=info
    uri ldap://directory.becarios.tonejito.info:389/
    ssl start_tls
    tls_cacertdir /etc/openldap/cacerts
    tls_reqcert allow
```

Una vez hecha la configuración reiniciamos nuestros servicios:

```shell
   /etc/init.d/nscd restart
   /etc/init.d/nslcd restart   
```
Realizamos si tenemos comunicación con LDAP ocupando en usuario anónimo
```shell
   ldapsearch -x 
```
Ahora procedemos a obtener los usuarios a través del siguiente comando y verificamos que estén los usuarios de LDAP

```shell
	getent passwd
```
Usuarios:
```shell
    Usuarios de Sistema:
    prueba1:x:506:506::/home/prueba1:/bin/bash
    prueba2:x:507:507::/srv/home/prueba2:/bin/bash
    prueba3:x:508:508::/srv/home/prueba3:/bin/bash
    
    Usuarios LDAP:
    dmartin:*:10018:10000:David Martin:/srv/home/dmartin:/bin/false
    jpacheco:*:10019:10000:Jesus Pacheco:/srv/home/jpacheco:/bin/false
    rduran:*:10020:10000:Ruben Duran:/srv/home/rduran:/bin/false
    jferrusca:*:10021:10000:Jorge Ferrusca:/srv/home/jferrusca:/bin/false
    lpablo:*:10022:10000:Lucia Pablo:/srv/home/lpablo:/bin/false
    vortiz:*:10023:10000:Valeria Ortiz:/srv/home/vortiz:/bin/false
    jcervantes:*:10024:10000:Juan Cervantes:/srv/home/jcervantes:/bin/false
    lmartinez:*:10025:10000:Luis Martinez:/srv/home/lmartinez:/bin/false
    oespinosa:*:10026:10000:Oscar Espinosa:/srv/home/oespinosa:/bin/false
    prodriguez:*:10027:10000:Pedro Rodriguez:/srv/home/prodriguez:/bin/false
    jjuarez:*:10028:10000:Jesika Juarez:/srv/home/jjuarez:/bin/false
    imanzano:*:10029:10000:Isaias Manzano:/srv/home/imanzano:/bin/false
    dtadeo:*:10030:10000:Diana Tadeo:/srv/home/dtadeo:/bin/false
    ileal:*:10031:10000:Ignacio Leal:/srv/home/ileal:/bin/false
    pgomez:*:10032:10000:Patricia Gomez:/srv/home/pgomez:/bin/false
    rhernandez:*:10033:10000:Ricardo Hernandez:/srv/home/rhernandez:/bin/false
    rvallejo:*:10034:10000:Rafael Vallejo:/srv/home/rvallejo:/bin/false
    slopez:*:10035:10000:Servando Lopez:/srv/home/slopez:/bin/false
```
OpenVPN
-------------------------------------------
Para la configuración de la VPN otorgada por el equipo correspondiente, primero
es necesario instalar el paquete `openvpn`
```shell
    yum install openvpn
```
Teniendo esto, se ejecuta el archivo de configuración ( otorgado por el equipo)
para tener acceso al tunel.
Este archivo se encuentra alojado en `/etc/openvpn/mail.ovpn`, aqui mismo
almacenamos el certificado que el equipo nos firmó.

Para que se tenga acceso a este tunel siempre, es necesario agregarlo a
un script que se ejecuta en cada inicio del SO. Para esto agregamos al archivo 
`/etc/rc.local` la línea
```shell
    screen -d -m openvpn --config /root/mail/mail.ovpn
```

DOVECOT
--------------------------------------------
Para instalarlo, usamos
```shell
    yum install dovecot
```
Se modifica el archivo `/etc/dovecot/dovecot.conf`
```shell
    !include conf.d/*.conf
    mail_location = maildir:/srv/home/%u/Maildir
    log_path = /var/log/dovecot
```
Se modifica el archivo `10-master`
```shell
    /etc/dovecot/conf.d/10-master
```
Y se agrega el siguiente contenido
```shell
   service imap-login {
        inet_listener imap {
          address = 127.0.0.1
          port = 143
        }
        inet_listener imaps {
        port = 993
        ssl = yes
        }
        service_count=1
        process_min_avail=1
    }
```
En el archivo `/etc/dovecot/conf.d/10-auth.conf` se tiene que hacer la siguiente
configuracion para agregar la autenticacion con LDAP a Dovecot
```shell
    #!include auth-system.conf.ext
    !include auth-ldap.conf.ext
```
En el mismo directorio, se edita el archivo `auth-ldap.conf.ext`, se le agrega
```shell
    passdb {
            driver = ldap
            args = /etc/dovecot/dovecot-ldap.conf.ext
    }
    userdb {
            driver = ldap
            args = /etc/dovecot/dovecot-ldap.conf.ext
    }
```
En el archivo `/etc/dovecot/dovecot-ldap.conf.ext` se agregan las lineas
```shell
    uris= ldap://directory.becarios.tonejito.info:389 ldaps://directory.becarios.tonejito.info:636
    dn= cn=admin,dc=directory,dc=becarios,dc=tonejito,dc=info
    dnpass= hola123,
    auth_bind = yes
    base= ou=user,dc=directory,dc=becarios,dc=tonejito,dc=info
    scope = subtree
    user_attrs = homeDirectory=home,uidNumber=uid,gidNumber=gid
    pass_attrs = uid=user
    pass_filter = (&(objectClass=*)(uid=%u))
    default_pass_scheme = SSHA
    ldap_version= 3
```

IPTABLES
------------------------------------------------------------
Para establecer reglas de firewall en el servidor, se modifico el archivo
`/etc/sysconfig/iptables.save` en el que se agregaron las siguientes lineas
```shell
    *filter
    :INPUT DROP [0:0]
    :FORWARD DROP [0:0]
    :OUTPUT ACCEPT [0:0]

    #Loopback
    -A INPUT -i lo -j ACCEPT
    -A OUTPUT -o lo -j ACCEPT
    -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT

    #DNS
    -A INPUT -p tcp --dport 53 -j ACCEPT
    -A OUTPUT -p tcp -m state --sport 53 --state ESTABLISHED,RELATED -j ACCEPT

    -A INPUT -p udp --dport 53 -j ACCEPT
    -A OUTPUT -p udp -m state --sport 53 --state ESTABLISHED,RELATED -j ACCEPT

    #SSH
    -A INPUT -p tcp --dport 22 -j ACCEPT
    -A OUTPUT -p tcp --sport 22 -j ACCEPT

    #Equipos
    -A INPUT -s tonejito.cf/32 -j ACCEPT
    -A OUTPUT -d tonejito.cf/32 -j ACCEPT

    -A INPUT -s becarios.tonejito.info/32 -j ACCEPT
    -A OUTPUT -d becarios.tonejito.info/32 -j ACCEPT

    -A INPUT -s becarios.priv.tonejito.info/32 -j ACCEPT
    -A OUTPUT -d becarios.priv.tonejito.info/32 -j ACCEPT

    -A INPUT -s database.becarios.tonejito.info/32 -j ACCEPT
    -A OUTPUT -d database.becarios.tonejito.info/32 -j ACCEPT

    -A INPUT -s directory.becarios.tonejito.info/32 -j ACCEPT
    -A OUTPUT -d directory.becarios.tonejito.info/32 -j ACCEPT

    -A INPUT -s storage.becarios.tonejito.info/32 -j ACCEPT
    -A OUTPUT -d storage.becarios.tonejito.info/32 -j ACCEPT

    -A INPUT -s web.becarios.tonejito.info/32 -j ACCEPT
    -A OUTPUT -d web.becarios.tonejito.info/32 -j ACCEPT

    #Red UNAM
    -A INPUT -s 132.247.0.0/16 -j ACCEPT
    -A OUTPUT -d 132.247.0.0/16 -j ACCEPT

    -A INPUT -s 132.248.0.0/16 -j ACCEPT
    -A OUTPUT -d 132.248.0.0/16 -j ACCEPT

    #VPN
    -A INPUT -s 10.0.8.0/24 -j ACCEPT
    -A OUTPUT -d 10.0.8.0/24 -j ACCEPT

    #HTTP/S
    -A INPUT -p tcp --dport 80 -j ACCEPT
    -A OUTPUT -p tcp --sport 80 -j ACCEPT
    -A INPUT -p tcp --dport 443 -j ACCEPT
    -A OUTPUT -p tcp --sport 443 -j ACCEPT

    #Puertos mail
    -A INPUT -p tcp -m multiport --dports 25,465,587,993 -j ACCEPT
    -A OUTPUT -p tcp -m multiport --sports 25,465,587,993 -j ACCEPT

    #Ping
    -A INPUT -p icmp -j ACCEPT
    -A OUTPUT -p icmp -j ACCEPT

    COMMIT
```
En este archivo se habilitaron unicamente los puertos que serian utilizados para
la conexion con el  servidor de correos (#Puertos mail), la conexion remota y al 
DNS (#DNS #SSH) y se restringio la comunicacion unicamente a los servidores del
dominio `becarios.tonejito.info` (#Equipo), conexiones unicamente desde la RED UNAM
(#Red UNAM) y a las VPN otorgadas por el equipo de VPN (#VPN).

En las reglas de DNS se agregaron las banderas -m y --state para permitir el
envio de correos a través de POSTFIX, en caso de no tener estas banderas, se puede
tener error en el envio de correos.

Las reglas para la conexión por HTTP/S se establecieron para la conexión con Apache
para el servicio de SquirrelMail.

Finalmente es necesario hacer énfasis en que el orden en que se establecen las reglas es
importante, de no seguir este orden en la configuración, se pueden tener errores con la
conexión a los dominios de VPN.

EPEL
----------------------------------------------------------------------
Tuvimos que instalar el repositorio EPEL, ya que este brinda de algunos
paquetes que no se encuentran en los repositorios convencionales de Red Hat.
Para hacer esto, basta con implementar los siguientes comandos:
```shell
    wget http://dl.fedoraproject.org/pub/epel/6/x86_64/epel-release-6-8.noarch.rpm
    rpm -ivh epel-release-6-8.noarch.rpm
```
Con el siguiente comando verificamos que EPEL este funcionando
```shell
	yum repolist
```
Se verá listado el repositorio recien instalado


Iniciar servicios al inicio de Sistema Operativo
----------------------------------------------------------
Para tener disponibles todas estas configuraciones realizadas, es necesario
agregar comandos al script /etc/rc.local
El archivo tiene el siguiente contenido
```shell
    #!/usr/bin/bash
    iptables-restore < /etc/sysconfig/iptables.save
    service httpd start
    service postfix start
    service dovecot start
    screen -d -m openvpn --config /etc/openvpn/mail.ovpn
```


SquirrelMail
----------------------------------------------------------------------
Para instalar `Squirrel` ocupamos las dependencias de EPEL
```shell
	yum --enablerepo=epel install squirrel
```
Una vez instalada nos dirigimos a correr el asistente de configuremos de squirrel
```shell
    /usr/share/squirrelmail/config/conf.pl
```
```shell
SquirrelMail Configuration : Read: config.php (1.4.0)
---------------------------------------------------------
Main Menu --
1. Organization Preferences
2. Server Settings
3. Folder Defaults
4. General Options
5. Themes
6. Address Books
7. Message of the Day (MOTD)
8. Plugins
9. Database
10. Languages
D. Set pre-defined settings for specific IMAP servers
C Turn color off
S Save data
Q Quit
Command >> 1   # select

SquirrelMail Configuration : Read: config.php (1.4.0)
---------------------------------------------------------
Organization Preferences
1. Organization Name: SquirrelMail
2. Organization Logo: ../images/sm_logo.png
3. Org. Logo Width/Height: (308/111)
4. Organization Title: SquirrelMail 
5. Signout Page :
6. Top Frame: _top
7. Provider link : http://squirrelmail.org/
8. Provider name: SquirrelMail

R Return to Main Menu
C Turn color off
S Save data
Q Quit

Command >> 5   # change log-out page
When users click the Sign Out button they will be logged out and
then sent to signout_page. If signout_page is left empty,
(hit space and then return) they will be taken, as normal,
to the default and rather sparse SquirrelMail signout page.

 : /squirrelmail     # change log-out page
 
SquirrelMail Configuration : Read: config.php (1.4.0)
---------------------------------------------------------
Organization Preferences
1. Organization Name: SquirrelMail
2. Organization Logo: ../images/sm_logo.png
3. Org. Logo Width/Height: (308/111)
4. Organization Title: SquirrelMail 
5. Signout Page: /webmail
6. Top Frame: _top
7. Provider link: http://squirrelmail.org/
8. Provider name: SquirrelMail

R Return to Main Menu
C Turn color off
S Save data
Q Quit

Command >> r   # back to main menu

SquirrelMail Configuration : Read: config.php (1.4.0)
---------------------------------------------------------
Main Menu --
1. Organization Preferences
2. Server Settings
3. Folder Defaults
4. General Options
5. Themes
6. Address Books
7. Message of the Day (MOTD)
8. Plugins
9. Database
10. Languages

D. Set pre-defined settings for specific IMAP servers
C Turn color off
S Save data
Q Quit

Command >> 2   # select

SquirrelMail Configuration : Read: config.php (1.4.0)
---------------------------------------------------------
Server Settings
General
-------
1. Domain: localhost
2. Invert Time: false
3. Sendmail or SMTP: Sendmail
A. Update IMAP Settings: localhost:143 (uw)
B. Change Sendmail Config: /usr/sbin/sendmail
R Return to Main Menu
C Turn color off
S Save data
Q Quit

Command >> 1   # change to your domain name

The domain name is the suffix at the end of all email addresses. If
for example, your email address is jdoe.com, then your domain
would be example.com.

[localhost]: becarios.tonejito.info   # input your domain name

SquirrelMail Configuration : Read: config.php (1.4.0)
---------------------------------------------------------
Server Settings
General
-------
1. Domain: becarios.tonejito.info
2. Invert Time: false
3. Sendmail or SMTP: Sendmail

A. Update IMAP Settings: localhost:143 (uw)
B. Change Sendmail Config: /usr/sbin/sendmail
R Return to Main Menu
C Turn color off
S Save data
Q Quit

Command >> A   # select

SquirrelMail Configuration : Read: config.php (1.4.0)
---------------------------------------------------------
Server Settings
General
-------
1. Domain: becarios.tonejito.info
2. Invert Time: false
3. Sendmail or SMTP: Sendmail
IMAP Settings
--------------
4. IMAP Server: localhost
5. IMAP Port : 143
6. Authentication type: login
7. Secure IMAP (TLS): false
8. Server software: uw
9. Delimiter : /

B. Update SMTP Settings: localhost:25
H. Hide IMAP Server Settings
R Return to Main Menu
C Turn color off
S Save data
Q Quit

Command >> 8   # select
Each IMAP server has its own quirks. As much as we tried to stick
to standards, it doesn't help much if the IMAP server doesn't follow
the same principles. We have made some work-arounds for some of
these servers. If you would like to use them, please select your
IMAP server. If you do not wish to use these work-arounds, you can
set this to "other", and none will be used.
   bincimap= Binc IMAP server
   courier= Courier IMAP server
   cyrus= Cyrus IMAP server
   dovecot= Dovecot Secure IMAP server
   exchange= Microsoft Exchange IMAP server
   hmailserver= hMailServer
   macosx = Mac OS X Mailserver
   mercury32= Mercury/32
   uw = University of Washingtons IMAP server
   gmail= IMAP access to Google mail (Gmail) accounts
   other= Not one of the above servers
   
[uw]: dovecot   # select Dovecot

SquirrelMail Configuration : Read: config.php (1.4.0)
---------------------------------------------------------
Server Settings
General
-------
1. Domain : becarios.tonejito.info
2. Invert Time : false
3. Sendmail or SMTP: Sendmail
IMAP Settings
--------------
4. IMAP Server: localhost
5. IMAP Port : 143
6. Authentication type: login
7. Secure IMAP (TLS): false
8. Server software: dovecot
9. Delimiter : /

B. Update SMTP Settings: localhost:25
H. Hide IMAP Server Settings
R Return to Main Menu
C Turn color off
S Save data
Q Quit

Command >> 9   # select
This is the delimiter that your IMAP server uses to distinguish between
folders. For example, Cyrus uses '.' as the delimiter and a complete
folder would look like 'INBOX.Friends.Bob', while UW uses '/' and would
look like 'INBOX/Friends/Bob'. Normally this should be left at 'detect'
but if you are sure you know what delimiter your server uses, you can
specify it here.

To have it autodetect the delimiter, set it to 'detect'.
[/]: detect   # specify detect

SquirrelMail Configuration : Read: config.php (1.4.0)
---------------------------------------------------------
Server Settings
General
-------
1. Domain: becarios.tonejito.info
2. Invert Time : false
3. Sendmail or SMTP: SMTP
IMAP Settings
--------------
4. IMAP Server : localhost
5. IMAP Port : 143
6. Authentication type: login
7. Secure IMAP (TLS): false
8. Server software: dovecot
9. Delimiter: detect

B. Update SMTP Settings: localhost:25
H. Hide IMAP Server Settings
R Return to Main Menu
C Turn color off
S Save data
Q Quit

Command >> r   # select

SquirrelMail Configuration : Read: config.php (1.4.0)
---------------------------------------------------------
Server Settings
General
-------
1. Domain : becarios.tonejito.info
2. Invert Time: false
3. Sendmail or SMTP: SMTP
SMTP Settings
-------------
4. SMTP Server: localhost
5. SMTP Port: 25
6. POP before SMTP: false
7. SMTP Authentication: none
8. Secure SMTP (TLS): false
9. Header encryption key:

A. Update IMAP Settings: localhost:143 (dovecot)
H. Hide SMTP Settings
R Return to Main Menu
C Turn color off
S Save data
Q Quit

Command >> 7   # select

If you have already set the hostname and port number, I can try to
automatically detect the mechanisms your SMTP server supports.
Auto-detection is *optional* - you can safely say "n" here.
Try to detect auth mechanisms? [y/N]: y   # yes (auto)
Trying to detect supported methods (SMTP)...
Testing none: SUPPORTED
Testing login: SUPPORTED
Testing CRAM-MD5: NOT SUPPORTED
Testing DIGEST-MD5: NOT SUPPORTED

What authentication mechanism do you want to use for SMTP connections?
none - Your SMTP server does not require authorization.
login - Plaintext. If you can do better, you probably should.
cram-md5 - Slightly better than plaintext.
digest-md5 - Privacy protection - better than cram-md5.

*** YOUR SMTP SERVER MUST SUPPORT THE MECHANISM YOU CHOOSE HERE ***
If you dont understand or are unsure, you probably want "none"

none, login, cram-md5, or digest-md5 [none]: login   # select login

SMTP authentication uses IMAP username and password by default.
Would you like to use other login and password for all SquirrelMail
SMTP connections? [y/N]: n

SquirrelMail Configuration : Read: config.php (1.4.0)
---------------------------------------------------------
Server Settings
General
-------
1. Domain : becarios.tonejito.info
2. Invert Time: false
3. Sendmail or SMTP: SMTP
SMTP Settings
-------------
4. SMTP Server: localhost
5. SMTP Port: 25
6. POP before SMTP: false
7. SMTP Authentication: login (with IMAP username and password)
8. Secure SMTP (TLS): false
9. Header encryption key:

A. Update IMAP Settings: localhost:143 (dovecot)
H. Hide SMTP Settings
R Return to Main Menu
C Turn color off
S Save data
Q Quit

Command >> r   # back to main menu

SquirrelMail Configuration : Read: config.php (1.4.0)
---------------------------------------------------------
Main Menu --
1. Organization Preferences
2. Server Settings
3. Folder Defaults
4. General Options
5. Themes
6. Address Books
7. Message of the Day (MOTD)
8. Plugins
9. Database
10. Languages

D. Set pre-defined settings for specific IMAP servers
C Turn color off
S Save data
Q Quit

SquirrelMail Configuration : Read: config.php (1.4.0)
---------------------------------------------------------
Main Menu --
1. Organization Preferences
2. Server Settings
3. Folder Defaults
4. General Options
5. Themes
6. Address Books
7. Message of the Day (MOTD)
8. Plugins
9. Database
10. Languages

D. Set pre-defined settings for specific IMAP servers
C Turn color off
S Save data
Q Quit

Command >> 4   # select

SquirrelMail Configuration : Read: config.php (1.4.0)
---------------------------------------------------------
General Options
1. Data Directory: /var/www/html/squirrelmail/prefs/
2. Attachment Directory: /var/www/html/squirrelmail/attach/
3. Directory Hash Level: 0
4. Default Left Size: 150
5. Usernames in Lowercase: false
6. Allow use of priority: true
7. Hide SM attributions: false
8. Allow use of receipts: true
9. Allow editing of identity: true
Allow editing of name: true
Remove username from header: false
10. Allow server thread sort: true
11. Allow server-side sorting: true
12. Allow server charset search: true
13. Enable UID support: true
14. PHP session name: SQMSESSID
15. Location base:
R Return to Main Menu
C Turn color off
S Save data
Q Quit

Command >> 7   # select

Hide SM attributions (y/n) [n]: y   # Yes

SquirrelMail Configuration : Read: config.php (1.4.0)
---------------------------------------------------------
General Options
1. Data Directory: /var/www/html/squirrelmail/prefs/
2. Attachment Directory: /var/www/html/squirrelmail/attach/
3. Directory Hash Level: 0
4. Default Left Size: 150
5. Usernames in Lowercase: false
6. Allow use of priority: true
7. Hide SM attributions: true
8. Allow use of receipts: true
9. Allow editing of identity: true
Allow editing of name: true
Remove username from header: false
10. Allow server thread sort: true
11. Allow server-side sorting: true
12. Allow server charset search: true
13. Enable UID support: true
14. PHP session name: SQMSESSID
15. Location base:
R Return to Main Menu
C Turn color off
S Save data
Q Quit

Command >> r   # back to main menu

SquirrelMail Configuration : Read: config.php (1.4.0)
---------------------------------------------------------
Main Menu --
1. Organization Preferences
2. Server Settings
3. Folder Defaults
4. General Options
5. Themes
6. Address Books
7. Message of the Day (MOTD)
8. Plugins
9. Database
10. Languages

D. Set pre-defined settings for specific IMAP servers
C Turn color off
S Save data
Q Quit

Command >> 8   # select

SquirrelMail Configuration : Read: config.php (1.4.0)
---------------------------------------------------------
Plugins
Installed Plugins
1. delete_move_next
2. squirrelspell
3. newmail
Available Plugins:
4. listcommands
5. filters
6. fortune
7. mail_fetch
8. message_details
9. compatibility
10. calendar
11. translate
12. sent_subfolders
13. empty_trash
14. spamcop
15. bug_report
16. abook_take
17. info
18. secure_login
19. administrator
R Return to Main Menu
C Turn color off
S Save data
Q Quit

Command >> 9   # add compatibility (number is different at an env)

SquirrelMail Configuration : Read: config.php (1.4.0)
---------------------------------------------------------
Plugins
Installed Plugins
1. delete_move_next
2. squirrelspell
3. newmail
4. compatibility
Available Plugins:
5. listcommands
6. filters
7. fortune
8. mail_fetch
9. message_details
10. calendar
11. translate
12. sent_subfolders
13. empty_trash
14. spamcop
15. bug_report
16. abook_take
17. info
18. secure_login
19. administrator
R Return to Main Menu
C Turn color off
S Save data
Q Quit

Command >> 13   # add empty_trash (number is different at an env)

SquirrelMail Configuration : Read: config.php (1.4.0)
---------------------------------------------------------
Plugins
Installed Plugins
1. delete_move_next
2. squirrelspell
3. newmail
4. compatibility
5. empty_trash
Available Plugins:
7. listcommands
8. filters
9. fortune
10. mail_fetch
11. message_details
12. calendar
13. translate
14. sent_subfolders
15. spamcop
16. bug_report
17. abook_take
18. info
19. administrator

R Return to Main Menu
C Turn color off
S Save data
Q Quit

Command >> q   # quit

You have not saved your data.
Save? [Y/n]: y   # save
Data saved in config.php
Exiting conf.pl.

You might want to test your configuration by browsing to
http://your-squirrelmail-location/src/configtest.php
Happy SquirrelMailing!
```
Ahora procedemos a recargar Apache para ver los cambios.
```shell
/etc/rc.d/init.d/httpd reload 
```


REFERENCIAS
-----------------------------------------------------------------------

Si se desea investigar mas a fondo sobre la configuracion de todos estos servicios,
las paginas consultadas para este proyecto fueron:
1. POSTFIX
- http://www.postfix.org/documentation.html
- http://www.postfix.org/BASIC_CONFIGURATION_README.html
- http://www.postfix.org/STANDARD_CONFIGURATION_README.html
- https://wiki.centos.org/HowTos/postfix
- https://www.digitalocean.com/community/tutorials/how-to-install-postfix-on-centos-6
- https://www.centos.org/docs/5/html/5.1/Deployment_Guide/s3-email-mta-postfix-conf.html
- https://help.ubuntu.com/community/Postfix

2. Configuracion de LDAP
- https://www.unixmen.com/configure-linux-clients-to-authenticate-using-openldap
- https://www.lisenet.com/2016/setup-ldap-authentication-on-centos-7
- https://wiki.archlinux.org/index.php/LDAP_authentication
- https://albertomolina.files.wordpress.com/2008/07/autenticacion_ldap.pdf

3. Scripts al inicio del SO
- https://www.cyberciti.biz/faq/linux-execute-cron-job-after-system-reboot/
- https://www.computerhope.com/unix/screen.htm

4. DOVECOT
- https://www.vennedey.net/resources/2-LDAP-managed-mail-server-with-Postfix-and-Dovecot-for-multiple-domains#dovecot
- https://www.digitalocean.com/community/tutorials/how-to-set-up-a-postfix-e-mail-server-with-dovecot
- https://www.linode.com/docs/email/postfix/email-with-postfix-dovecot-and-mysql/

5. IPTABLES
- https://ubuntuforums.org/showthread.php?t=1260560
- https://www.digitalocean.com/community/tutorials/how-to-set-up-a-basic-iptables-firewall-on-centos-6

6. EPEL
- https://fedoraproject.org/wiki/EPEL/es
- https://www.comoinstalarlinux.com/como-instalar-el-repositorio-epel-en-centos/

7. SQUIRREL 
- https://www.rosehosting.com/blog/how-to-install-squirrelmail-on-centos-6
- https://www.server-world.info/en/note?os=CentOS_6&p=httpd&f=17
- https://www.unixmen.com/install-postfix-mail-server-with-dovecot-and-squirrelmail-on-centos-6-4

Colaboradores
-------------------------------------------------------------------------

+ Espinosa Curiel Oscar 
+ Juárez Méndez Jesika
+ Manzano Cruz Isías Abraham
+ Rodríguez Gallardo Pedro Alejandro
